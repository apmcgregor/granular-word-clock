# Granular Word Clock

A time function that turns the local time into a colloquial word string of varying granularity.
E.G. 10:52 -> Ten to Eleven in the morning OR Eleven in the morning OR Morning.
